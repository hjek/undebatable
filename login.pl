:- module(login, [
	      get_user/1,
	      login_form//1,
	      signup_form//1,
	      login_button//0
	  ]).

:- use_module(library(persistency)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_parameters)).
:- use_module(library(http/http_header)).
:- use_module(library(http/http_client)).
:- use_module(library(http/html_write)).
:- use_module(library(http/http_session)).

:- use_module(captcha).

:- persistent user_pwhash(user, hash).
:- persistent user_email(user, email).
:- persistent user_created(user, created).
:- persistent debug(info).
:- make_directory_path('www').
:- db_attach('www/login.pl', []).

% enable/disable captcha
captcha_enabled(true).
%
% TODO: should be able to change email
% change password
% delete user.
% change username (?)
% export user data

:- http_set_session_options([cookie(login)]).

:- http_handler(root(login),
		login_handler(Method),
		[method(Method), methods([get,post])]).

:- http_handler(root(signup),
		signup_handler(Method),
		[method(Method), methods([post])]).

:- http_handler(root(logout),
		logout_handler(Method),
		[method(Method),
		 methods([post])]).

valid_email(E):-
    % blank email is fine
    re_match('^.*@.*$|^$', E).

valid_name(N) :-
    % more than 1 asci char
    re_match('^[a-zA-Z0-9]+$', N).

valid_password(P) :-
    % passwords: >1 char
    re_match('.+',P).

user_password(User, Password):-
    % the hash must be instantiated first
    user_pwhash(User, Hash),
    % then the hash can be checked with crypto_password_hash
    crypto_password_hash(Password, Hash).

reset_user_password(User, Password):-
    % if someone needs their password reset
    crypto_password_hash(Password, Hash),
    retractall_user_pwhash(User,_),
    assert_user_pwhash(User,Hash).

create_user(User, Email, Password):-
    % check if OK
    not(user_created(User,_)),
    valid_email(Email),
    valid_password(Password),
    valid_name(User),
    % create the user
    crypto_password_hash(Password, Hash),
    get_time(Now),
    assert_user_pwhash(User, Hash),
    assert_user_email(User, Email),
    assert_user_created(User, Now).

% # Web login
login_form(Then) -->
    html(
	div([
		   form([action(login), method(post)],[
			    div([
				       p(b('log in')),
				       label(for(user), name),
				       input([name(user), type(text), required])]),
			    div([
				       label(for(password), password),
				       input([name(password), type(password), required]),
				       input([name(then), type(hidden), value=Then])]),
			    input([type(submit), value('log in')])])])).

captcha -->
    { captcha_enabled(true),
      captcha(Captcha,_,Image) },
    html(
	div([
		   div([img([src(Image),id(captcha),alt(captcha)])]),
		   label(for(captcha), 'please enter above text'),
		   input([name(captcha_q), type(hidden), value(Captcha)]),
		   input([name(captcha_a), autocomplete(off), required])])).

captcha -->
    % if captcha isn't required
    [].

signup_form(Then) -->
    html(
	form([action(signup), method(post)],[
		 p(b('create a new user')),
		 div([
			    label(for(user), name),
			    input([name(user), type(text), pattern('[a-zA-Z0-9]+'), required])]),
		 div([
			    label(for(email), email),
			    input([name(email), type(email)])]),
		 div([
			    label(for(password), password),
			    input([name(password), type(password), required])]),
		 \captcha,
		 input([name(then), type(hidden), value(Then)]),
		 input([type(submit), value('sign up')])])).

login_handler(get, Request):-
    ( member(referer(Referer), Request) -> Then=Referer; Then='/'),
    %  http_parameters(Request, [error(Error, [default('')])]),
    app:reply_app_page(
	login,
	%TODO: news_page is the only dependency [login] has on [news]
	[\login_form(Then),
	 hr(''),
	 \signup_form(Then)]).

login_handler(post, Request):-
    http_parameters(Request,[
			user(User,[]),
			password(Password,[]),
			then(Then,[default("")])]),
    (user_password(User, Password) ->
	 http_session_assert(user(User)),
	 http_redirect(see_other, Then, Request);
     % TODO: better errors
     throw(http_reply(not_acceptable('Invalid login.')))).
%throw(http_reply(forbidden(root(login))))).

signup_handler(post, Request):-
    http_parameters(Request,[
			user(User,[]),
			email(Email,[default("")]),
			password(Password,[]),
			then(Then,[default("")]),
			captcha_q(Captcha_q,[optional(true)]),
			captcha_a(Captcha_a,[optional(true)])]),

    (not(valid_name(User)) ->
	 throw(http_reply(not_acceptable('Invalid name.')));
     true),

    (user_created(User,_) ->
	 throw(http_reply(not_acceptable('Username already taken.')));
     true),

    (not(valid_email(Email)) ->
	 throw(http_reply(not_acceptable('Invalid email.')));
     true),

    (not(valid_password(Password)) ->
	 throw(http_reply(not_acceptable('Invalid password.')));
     true),

    (captcha_enabled(true), not(captcha(Captcha_q,Captcha_a,_)) ->
	 throw(http_reply(not_acceptable('Captcha failed.')));
     true),

    create_user(User,Email,Password),
    http_session_assert(user(User)),
    http_redirect(see_other, Then, Request).

logout_handler(post, Request):-
    ( member(referer(Referer), Request) -> Then=Referer; Then=root(.)),
    http_session_retract(user(_)),
    http_redirect(see_other, Then, Request).

logout_handler(post, Request):-
    http_redirect(see_other, '/', Request).

get_user(User):-
    http_session_data(user(User)).

login_button -->
    { get_user(_) },
    html(
	form([action(logout), method(post)],
	     input([class(login), type(submit),value(logout)]))).

login_button -->
    html(
	form(action(login),
	     input([class(login), type(submit), value('log in / sign up')]))).


