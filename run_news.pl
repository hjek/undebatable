#!/usr/bin/env -S swipl -q --stack_limit=4m
:- use_module(news).
:- current_prolog_flag(argv, Argv),
   ( Argv=['-p',Port_atom] -> true; Port=8080 ),
   atom_number(Port_atom,Port),
   app:serve(Port),
   string_concat("http://localhost:",Port,Url),
   www_open_url(Url).
