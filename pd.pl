#!/usr/bin/env -S swipl -q
% -----------------------------------------------------------
% Prodata
% Write Puredata patches in Prolog
%
% © 2020 Pelle Hjek Møller Kirkeby
% GPL v3 or later
% -----------------------------------------------------------
%
% Pd file format description:
% https://puredata.info/docs/developer/PdFileFormat
% (Only a subset is implemented here)

:- module(pd, [run_current_patch/0, clear_current_patch/0, (=>)/2, (:=)/2]).
:- set_prolog_flag(stack_limit,4000000).


% % FUNDAMENTAL PD PATCH RECORD STRUCTURE
% % =====================================

patch([]) --> [].

patch([R|Rs]) -->
    R, patch(Rs).

end() --> [";\r\n"].

record(canvas(position(X,Y), size(W,H), name(N))) -->
    ["#N canvas ", X, " ", Y, " ", W, " ", H, " ", N], end.

record(object(type(T), position(X,Y), Ps)) -->
    ["#X ", T, " ", X, " ", Y], parameters(Ps), end.

record(connect(source(Source, Outlet), sink(Sink, Inlet))) -->
    ["#X connect ", Source, " ", Outlet, " ", Sink, " ", Inlet], end.

parameters([]) --> [].
parameters([P|Ps]) --> [" ", P], parameters(Ps).

records_patch(Rs,Pd) :-
    % relation between a list of records and a well-formed Pd patch
    phrase(patch(Rs),P),
    atomic_list_concat(P,Pd).



% % STATEFUL OBJECT CREATION
% % ========================
:- dynamic state/1.

ensure_canvas :-
    state(record(canvas(_,_,_))),!.

ensure_canvas :-
    format("Created canvas.\n",[]),
    assert(state(record(canvas(position(0,0), size(400,400), name(my_canvas))))).
    
gen_id(Id) :-
    findall(_, state(record(object(_,_,_))), Os),
    length(Os, Id).

object(obj, Id, Name, Ps) :-
    ensure_canvas(),
    gen_id(Id),
    % can't use tildes in prolog atoms without quoting them, so allow underscore as alternative
    re_replace("_", "~", Name, Name1),
    format("Created object: ~w.\n", [Name1]),
    assert(state(record(object(type(obj), position(0,0), [Name1|Ps])))).

object(msg, Id, Text) :-
    string(Text),
    ensure_canvas(),
    gen_id(Id),
    format("Created message: ~w.\n", Text),
    assert(state(record(object(type(msg), position(0,0), Text)))).

connect(source(Source, Outlet), sink(Sink, Inlet)) :-
% TODO: Declaratively thinking, objects should really be able to be connected "before" they're created
    format("Connected ~w:~w to ~w:~w.\n", [Source, Outlet, Sink, Inlet]),
    assert(state(record(connect(source(Source, Outlet), sink(Sink, Inlet))))).

current_patch(Pd) :-
    % the current state as a well-formed Pd patch
    findall(S,state(S),Ss),
    records_patch(Ss,Pd).

clear_current_patch :-
    format("Patch cleared.\n", []),
    retractall(state(_)).



% % SYNTACTIC SUGAR
% % ===============

:- op(100, yfx, user:(:=)).

Id := [Name|Ps] :-
    object(obj, Id, Name, Ps),!.

Id := Text :-
    object(msg, Id, Text),!.

% Why bother with floatatom, symbolatom...?

:- op(200, xfx, user:(=>)).

[Source:Outlet] => [Sink:Inlet] :-
    connect(source(Source, Outlet), sink(Sink, Inlet)).



% % EXECUTION OF PATCHES IN Pd
% % ==========================
% (is this necessary?)

:- dynamic current_pd_process/1.

run_patch(Pd) :-
    current_pd_process(PID),
    % if pd is running
    catch(
	process_wait(PID, timeout, [timeout(0)]),
	_E,
	fail),
    % TODO: process_kill doesn't appear to work ...
    process_kill(PID),
    % ... so killing all Pd processes, just to be sure, for now.
    format("killing ~w!\n",[PID]),
    kill(PID,9),
    %shell("killall -9 pd"),
    run_patch(Pd),!.

run_patch(Pd) :-
    tmp_file_stream(text, File, Stream),
    write(Stream, Pd),
    close(Stream),
    process_create(path(pd), [File], [stderr(null), process(PID)]),
    format("Opening patch (~w) in Pd...\n",[File]),
    assert(current_pd_process(PID)).
%    delete_file(File). % maybe clean up?

run_current_patch :-
    current_patch(Pd),
    run_patch(Pd).
