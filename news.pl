:- module(news,[
	      item_title/2,
	      news_page//1,
	      item_form//1,
	      delete_form//1,
	      items_tree//3,
	      item_tree//3,
	      pagination//3,
	      search_form//0
	  ]).

% item: expanded? y/n
% item: show_text? y/n

% Start the server in EdiProlog
% ?- app:serve(8000). 

:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_parameters)).
:- use_module(library(http/http_header)).
:- use_module(library(http/http_client)).
:- use_module(library(http/html_head)).
:- use_module(library(http/html_write)).
:- use_module(library(http/http_wrapper)).
:- use_module(library(clpfd)).
:- use_module(library(persistency)).

arrow(up, fresh, △).
arrow(down, fresh, ▽).
arrow(up, voted, ▲).
arrow(down, voted, ▼).
arrow(_, self, ○).


site_name('news').
site_url('example.com').

top_label(recent_link).
top_label(replies_link).
top_label(upload_link).
top_label(submit_link).

% # Load modules
:- use_module(app).
:- use_module(login).
:- use_module(markdown).
:- use_module(about).
:- use_module(upload).

:- persistent debug(info).
:- persistent item_title(id, title).
:- persistent item_url(id, url).
:- persistent item_tags(id, tags).
:- persistent item_text(id, text).
:- persistent item_author(id, user).
:- persistent item_created(id, time).
:- persistent parent_child(parent_id, child_id).
:- persistent user_vote_item(user, how, item).
:- persistent user_seen(user, item).

% Load DB _after_ declaring all persistent predicates
:- make_directory_path('www').
:- db_attach('www/news.pl', []).

% # HTTP routing

:- http_handler(root(.), news_handler, []).

:- http_handler(root(item), item_handler, []).

:- http_handler(root(replies), replies_handler, []).

:- http_handler(root(submit),
		submit_handler(Method),
		[method(Method), methods([get,post])]).

:- http_handler(root(reply),
		reply_handler(Method),
		[method(Method), methods([get,post])]).

:- http_handler(root(edit),
		edit_handler(Method),
		[method(Method), methods([get])]).

% Note: can't backtrack on reading post request data so need separate edit handlers
:- http_handler(root(edit/story),
		edit_handler(story, Method),
		[method(Method), methods([post])]).

:- http_handler(root(edit/comment),
		edit_handler(comment, Method),
		[method(Method), methods([post])]).

:- http_handler(root(delete),
		delete_handler(Method),
		[method(Method), methods([get,post])]).

:- http_handler(root(vote),
		vote_handler(Method),
		[method(Method), methods([post])]).

param(title, [atom, length =< 80, length >= 1]).
param(id, [nonneg]).
param(to, [nonneg]).
param(parent, [nonneg]).
param(url, [atom, default('')]).
param(tags, [atom, default('')]).
%param(tags, [list(atom)]).
param(text, [atom, default('')]).


% # Items
% ## Reasoning about items
ancestor_descendant(A, B):-
    parent_child(A, B).
ancestor_descendant(A, B):-
    parent_child(A, C),
    ancestor_descendant(C, B).

root_item(Root, Item) :-
    ancestor_descendant(Root, Item),
    not(ancestor_descendant(_, Root)).

root_item(Item, Item) :-
    not(ancestor_descendant(_, Item)).

% TODO: use these more?
item_type(Item, Type) :-
    item_title(Item,_),
    Type = story.

item_type(Item, Type) :-
    parent_child(_,Item),
    item_text(Item,_),
    Type = comment.

user_reply(User, Item) :-
    ground(User),
    parent_child(Parent, Item),
    item_author(Parent, User).

user_notices(User, Notices) :-
    findall(Item,
	    (user_reply(User, Item), not(user_seen(User, Item))),
	    Items),
    length(Items, Notices).

item_domain(Item,Domain) :-
    item_url(Item,Url),
    % uri_components can leave Domain ungrounded on empty Url
    not(Url = ''),
    uri_components(Url, uri_components(_, Domain, _, _, _)).

% ### Time and amounts
plural(Noun, Quantity, Plural, Form) :-
    ( Quantity = 1 -> Form = Noun; Form = Plural).

plural(Noun, Quantity, Form) :-
    atomics_to_string([Noun, 's'], Plural),
    plural(Noun, Quantity, Plural, Form).

age(Item, Seconds) :-
    get_time(T0_),
    item_created(Item, T1_),
    floor(T0_,T0),
    floor(T1_,T1),
    Seconds #= T0-T1.

recent(Item, Seconds) :-
    age(Item, Seconds_),
    Seconds #= - Seconds_.

seconds_time(Seconds0, [Days,Hours,Minutes,Seconds]) :-
    % translate seconds into days/hours/minutes/seconds
    floor(Seconds0,Seconds1),
    Seconds #= Seconds1 mod 60,
    Minutes0 #= Seconds1 // 60,
    Minutes #= Minutes0 mod 60,
    Hours0 #= Minutes0 // 60,
    Hours #= Hours0 mod 24,
    Days #= Hours0 // 24.

text_age(Item, Age) :-
    % the age of an item in text
    age(Item,Seconds0),
    seconds_time(Seconds0, [Days,Hours,Minutes,Seconds]),
    (Days > 0 -> Age = [Days, ' ',  Days_form],
		 plural('day', Days, Days_form);
     (Hours > 0 -> Age = [Hours, ' ', Hours_form],
		   plural('hour', Hours, Hours_form) ;
      (Minutes > 0 -> Age = [Minutes, ' ', Minutes_form],
		      plural('minute', Minutes, Minutes_form);
       Age = [Seconds, ' ', Seconds_form],
       plural('second', Seconds, Seconds_form)))).

parent_children(Item, Children) :-
    findall(Child, parent_child(Item, Child), Children).

% ### Ranking / scoring / sorting
votes(Item, How, Count) :-
    findall(Item, user_vote_item(_, How, Item), Votes),
    length(Votes,Count).

item_score(Item, Points) :-
    ground(Item),
    votes(Item, up, Ups),
    votes(Item, down, Downs),
    Points #= Ups-Downs.

item_rank(Item, Value) :-
    % HN-ish frontpage ranking
    % https://medium.com/hacking-and-gonzo/how-hacker-news-ranking-algorithm-works-1d9b0cf2c08d
    % Don't need to subtract 1 point becuz you can't vote on own item
    % Perhaps add random noise to rank? https://danluu.com/randomize-hn/
    item_score(Item, Points),
    Gravity #= 2,
    age(Item, Time_),
    floor(Time_,Time),
    % inc Time to avoid div by zero on brand new items
    Value #= (Points * 10^20) // (Time+1) ^ Gravity.

user_karma(User, Sum) :-
    findall(Score, (item_author(Item, User), item_score(Item, Score)), Scores),
    foldl(plus,Scores,0,Sum).

sort_by(Items, Criteria, Sorted_unique) :-
    map_list_to_pairs(Criteria, Items, Pairs),
    sort(1, @>=, Pairs, Sorted_pairs),
    pairs_values(Sorted_pairs, Sorted),
    list_to_set(Sorted, Sorted_unique).

news_handler(Request):-
    (get_user(User); true),

    http_parameters(Request,[
			page(Current_page, [nonneg, default(1)]),
			% "query" is too generic. need param for each attribute ... maybe
			query(Query, [optional(true)]),
			domain(Domain, [optional(true)]),
			by(Author, [optional(true)]),
			type(Types, [list(atom)]),
			tag(Tags, [list(string)]),
			replies(Replies, [optional(true)]),
			sort_by(Order, [default(item_rank)]),
			per_page(Per_page, [nonneg, default(8)])
		    ],[form_data(Data)]),

    findall(Item,(
		( ground(Author) -> item_author(Item, Author); true),
		( ground(Domain) -> item_domain(Item, Domain); true),
		( ground(Replies) -> user_reply(User, Item); true),
		( ground(Query) ->
		  (item_title(Item, Title), re_match(Query/i, Title));
		  true),
		( Types \= [] -> item_type(Item, Type), member(Type, Types);
		  item_type(Item, story)),
		( Tags \= [] -> item_tags(Item, Tags0), member(Tag, Tags), member(Tag, Tags0); true)
	    ), Items),

    sort_by(Items, Order, Sorted_items),

    page_of_items(Sorted_items, Current_page, Per_page, Page_of_items, Total_pages),

    reply_news_page(
	top,[ol(\items_tree(Page_of_items, User, item_list)),
	     \pagination(Current_page, Total_pages, Data)]).

%% PAGE ELEMENTS

site_name -->
    { site_name(Name) },
    html(span(b(a([href('/')], Name)))).

submit_link -->
    html(span(a(href(submit), submit))).

recent_link -->
    { http_link_to_id(news_handler, [sort_by(item_created)], Url) },
    html(span(a(href(Url), recent))).

replies_link -->
    { get_user(User),
      ground(User),
      user_notices(User, Notices),
      http_link_to_id(news_handler,
		      [replies(true), type(comment), sort_by(item_created)], Url) },
    html(span(a(href(Url), [replies, ' (',Notices,')']))).

replies_link -->
    [].

top_bar -->
    { findall(Label,top_label(Label),Labels) },
    html(div(id(top),[
		 span(id(labels),\spaced([site_name|Labels])),
		 \user_form])).

search_form -->
    html(div(id(search),
	     form(
%		 action('/'),[
		 [
		     input([type(search),name(query)]),
		     input([type(submit),value(search)])
		 ]))).

bottom_bar -->
    html(div(id(bottom),
	     a(href(about),about))).

user_form -->
    { get_user(User),
      user_karma(User, Karma),
      http_link_to_id(news_handler, [by(User)], Url) },
    html(span(class(user),[
		  a(href(Url), span([User, ' (', Karma, ')'])),
		  \login_button])).

user_form -->
    html(div(class(user), \login_button)).

news_page(Body) -->
    html([
		\top_bar,
		\search_form,
		hr(''),
		div(class(main), Body),
		hr(''),
		\bottom_bar
	    ]).

reply_news_page(Title, Body) :-
    reply_app_page(Title, \news_page(Body)).

%% ITEM FORM

% TODO: merge form for new & edit story
% story form
item_form(new) -->
    html(
	form([action(submit),
	      method(post)
	     ],[
		 div([label(for(title), title),
		      input([name(title), type(text), required])]),
		 div([label(for(url), url),
		      input([name(url), type(url)])]),
		 %        iframe(src(upload),''),
		 % TODO: should be [select multiple] input
		 div([label(for(text), text),
		      textarea([name(text)], '')]),
		 input([type(submit), value(submit)])])).

% reply form
item_form(to(To)) -->
    html(
	form([action(reply), method(post)],
	     div([input([name(to), type(hidden), value(To)]),
		  div(textarea([name(text)], '')),
		  input([type(submit),value(reply)])]))).

% edit story
item_form(edit(Item)) -->
    {
	item_title(Item, Title),
	item_url(Item, Url),
	item_tags(Item, Tags),
	item_text(Item, Text)
    },
    html(
	form([action('edit/story'), method(post)],[
		 input([name(id), type(hidden), value(Item)]),
		 div([label(for(title),title),
		      input([name(title), type(text), value(Title), required])]),
		 div([label(for(url),url),
		      input([name(url), type(url), value(Url)])]),
		 div([label(for(tags), tags),
		      input([name(tags), type(text), value(Tags)])]),
		 div([label(for(text),text),
		      textarea([name(text)], Text)]),
		 input([type(submit),value(update)])])).

% edit comment
item_form(edit(Item)) -->
    {
	parent_child(_, Item),
	item_text(Item, Text)
    },
    html([form([action('edit/comment'), method(post)],
	       div([input([name(id), type(hidden), value(Item)]),
		    div(textarea([name(text)], Text)),
		    button(class(primary),update)]))]).

% ## Creating items

% ### Processing items (wether new submissions or replies)

get_id(Id) :-
    findall(Item, item_created(Item,_), Items),
    length(Items, Length),
    Id #= Length+1.

user_can_edit_item(User, Item):-
    % same rules for editing and deleting
    user_can_delete_item(User, Item).

user_can_delete_item(User, Item) :-
    % you can delete your own items
    item_author(Item, User).

process_item(User, Data, Id) :-
    % edit a story
    % item exists if an Id is passed here.
    ground(User),
    ground(Id),
    % is user allowed to edit?
    user_can_edit_item(User,Id),
    member(title(Title), Data),
    member(url(Url), Data),
    member(tags(Tags_unsplit), Data),
    member(text(Text), Data),
    split_string(Tags_unsplit, ' ', ' ,', Tags),
    retract_item_title(Id, _),
    retract_item_url(Id, _),
    retract_item_tags(Id, _),
    retract_item_text(Id, _),
    assert_item_title(Id, Title),
    assert_item_url(Id, Url),
    assert_item_tags(Id, Tags),
    assert_item_text(Id, Text).

process_item(User, Data, Id) :-
    % edit a comment
    % item exists if an Id is passed here.
    ground(User),
    ground(Id),
    % it is a reply
    parent_child(_, Id),
    % is user allowed to edit?
    user_can_edit_item(User,Id),
    member(text(Text), Data),
    retract_item_text(Id, _),
    assert_item_text(Id, Text).

process_item(User, Data, Id) :-
    % submit a story
    ground(User),
    member(title(Title), Data),
    member(url(Url), Data),
    member(text(Text), Data),
    member(tags(Tags_unsplit), Data),
    split_string(Tags_unsplit, ' ', ' ,', Tags),
    get_id(Id),
    get_time(Now),
    assert_item_author(Id, User),
    assert_item_created(Id, Now),
    assert_item_title(Id, Title),
    assert_item_url(Id, Url),
    assert_item_tags(Id, Tags),
    assert_item_text(Id, Text).

process_item(User, Data, Id) :-
    % submit a comment
    ground(User),
    member(parent(Parent), Data),
    member(text(Text), Data),
    get_id(Id),
    get_time(Now),
    assert_parent_child(Parent, Id),
    assert_item_author(Id, User),
    assert_item_text(Id, Text),
    assert_item_created(Id, Now).

delete_item(User, Id) :-
    % delete a story or reply
    ground(User),
    user_can_delete_item(User, Id),
    retractall_item_title(Id, _),
    retractall_item_url(Id, _),
    retractall_item_author(Id, _),
    retract_item_text(Id, _).


% ### Submitting a new story

submit_handler(get, Request):-
    (get_user(_) ->
	 reply_news_page(
	     submit,
	     \item_form(new));
     http_redirect(see_other, root(login), Request)).

submit_handler(post, Request):-
    (get_user(User) ->
	 http_parameters(Request,
			 [title(Title),
			  url(Url),
			  tags(Tags),
			  text(Text)],
			 [attribute_declarations(param)]
			),
	 % if there's a dupe, just go to that
	 (item_url(Dupe,Url), Url \= '' ->
	      http_link_to_id(item_handler, [id(Dupe)], Dupe_url),
	      http_redirect(see_other, Dupe_url, Request); true),
	 process_item(
	     User,
	     [title(Title),
	      url(Url),
	      tags(Tags),
	      text(Text)],
	     Id),
	 http_link_to_id(item_handler, [id(Id)], Then),
	 http_redirect(see_other, Then, Request);
     throw(http_reply(forbidden('/submit')))).

% ### Replying to an existing item

reply_handler(get, Request):-
    http_parameters(Request,
		    [to(To)],
		    [attribute_declarations(param)]),
    (get_user(User) ->
	 reply_news_page(
	     reply,
	     [ul(\item_tree(To, User, stub)),
	      \item_form(to(To))]);
     http_redirect(see_other, root(login), Request)).

reply_handler(post, Request):-
    (get_user(User) ->
	 http_parameters(Request,
			 [to(To),
			  text(Text)],
			 [attribute_declarations(param)]),
	 process_item(
	     User,
	     [text(Text),
	      parent(To)],
	     _),
	 % 'Id' is the new reply/item
	 % 'To' is the item being responded to
	 http_link_to_id(item_handler, [id(To)], Url),
	 http_redirect(see_other, Url, Request);
     throw(http_reply(forbidden(root(submit))))).

% ### Editing an existing item

edit_handler(get, Request):-
    http_parameters(Request,
		    [id(Id)],
		    [attribute_declarations(param)]),
    (get_user(User) ->
	 (reply_news_page(
	      reply,
	      [ul(\item_tree(Id, User, stub)),
	       \item_form(edit(Id))]));
     http_redirect(see_other, root(login), Request)).

edit_handler(story, post, Request):-
    (get_user(User) ->
	 http_parameters(Request,
			 [id(Id),
			  title(Title),
			  url(Url),
			  tags(Tags),
			  text(Text)],
			 [attribute_declarations(param)]),
	 process_item(
	     User,
	     [title(Title),
	      url(Url),
	      tags(Tags),
	      text(Text)],
	     Id),
	 http_link_to_id(item_handler, [id(Id)], Then),
	 http_redirect(see_other, Then, Request);
     throw(http_reply(forbidden(root(edit))))).

edit_handler(comment, post, Request):-
    (get_user(User) ->
	 http_parameters(Request,
			 [id(Id),
			  text(Text)],
			 [attribute_declarations(param)]),
	 process_item(
	     User,
	     [text(Text)],
	     Id),
	 http_link_to_id(item_handler, [id(Id)], Then),
	 http_redirect(see_other, Then, Request);
     throw(http_reply(forbidden(root(edit))))).

% ### Deleting items

delete_handler(get, Request):-
    http_parameters(Request,
		    [id(Item)],
		    [attribute_declarations(param)]),
    (get_user(User) ->
	 reply_news_page(
	     delete,
	     [ul(\item_tree(Item, User, stub)),
	      \delete_form(Item)]);
     throw(http_reply(forbidden(root(delete))))).

delete_handler(post, Request):-
    (get_user(User) ->
	 http_read_data(Request, Data, []),
	 Data = [id = Item_raw, answer = Answer|_],
	 atom_number(Item_raw, Item),
	 ( Answer = yes ->
	   root_item(Root, Item),
	   delete_item(User,Item),
	   http_link_to_id(item_handler, [id(Root)], Root_Url),
	   % Note: If Root is itself, you just get a blank page (which is perhaps alright)
	   http_redirect(see_other, Root_Url, Request);
	   http_link_to_id(item_handler, [id(Item)], Url),
	   http_redirect(see_other, Url, Request)),
	 throw(http_reply(forbidden(root(delete))))).

delete_form(Item) -->
    html([
		form([action(delete), method(post)], div([
								div('Do you want this item to be deleted?'),
								input([name(id), type(hidden), value(Item)]),
								button([value(yes), name(answer), class(danger)], yes),
								button([value(no), name(answer)], no)
							    ]))]).



% ## Handling requests for viewing items

item_handler(Request):-
    http_parameters(Request,
		    [id(Id)],
		    [attribute_declarations(param)]),
    % what if the root is deleted and has no title?
    %root_item(Root,Item),
    %title(Root, Title),
    ( get_user(User); true ),
    item_created(Id,_),
    reply_news_page(
	item,
	ul(\item_tree(Id, User, root))).

item_handler(Request):-
    % if the item is not there, or has been deleted
    http_parameters(Request,
		    [id(Id)],
		    [attribute_declarations(param)]),
    http_link_to_id(item_handler, [id(Id)], Url),
    throw(http_reply(not_found(Url))).

% ## Viewing items

items_tree([], _, _) -->
    [].

items_tree([Item|Items], User, Tree) -->
    item_tree(Item, User, Tree),
    items_tree(Items, User, Tree).

item_tree(Item, User, stub) -->
    item(Item, User, stub).

item_tree(Item, User, item_list) -->
    item(Item, User, item_list).

item_tree(Item, User, Tree) -->
    { parent_children(Item, Children),
      sort_by(Children, item_rank, Sorted_children) },
    html([
		\item(Item, User, Tree),
		ul(\items_tree(Sorted_children, User, tree))]).

item(Item, User, item_list) -->
    % show abbreviated item for title listings
    { item_type(Item, story) },
    html(li(class(item),[
		\vote_arrows(Item),
		section([
			       \title_line(Item),
			       \item_line(Item, User, item_list)
			   ])])).

item(Item, User, Tree) -->
    % show full item
    { item_text(Item, Text),
      markdown_html(Text, Html),
      (ground(User),
       not(user_seen(User, Item)) ->
	   assert_user_seen(User, Item);
       true)
    },
    html(li(class(item),[
		\vote_arrows(Item),
		section([
			       \title_line(Item),
			       \item_line(Item, User, Tree),
			       div(Html)
			   ])])).

item(Item, User, _) -->
    % item's been deleted?
    html(li(class(item),[
		\vote_arrows(Item),
		section([
			       s(\title_line(Item)),
			       s(\item_line(Item, User, _)),
			       div(deleted)
			   ])])).

% ### Sub-elements that make up an item when viewed
spaced([]) -->
    [].

spaced([E|Es]) -->
    % show elements with spaces in between them
    E,
    [' '],
    spaced(Es).

domain_link(Item) -->
    { item_domain(Item, Domain),
      ground(Domain),
      http_link_to_id(news_handler, [domain(Domain)], Url) },
    html(span([' (', a(href(Url), Domain), ') '])).

domain_link(_Item) -->
    [].

tag_links([]) -->
    [].

tag_links([Tag|Tags]) -->
    { http_link_to_id(news_handler, [tag(Tag)], Url) },
    html([a([class(tag),href(Url)],Tag), ' ', \tag_links(Tags)]).

title_line(Item) -->
    { item_title(Item, Title),% item_tags(Item,Tags),
      (item_url(Item,Item_url), Item_url \= '' ->
	   item_url(Item,Url);
       http_link_to_id(item_handler, [id(Item)], Url)) },
    html([b(a([class(title_line), href(Url)], Title)),
	  ' ',
%	  \tag_links(Tags),
	  \domain_link(Item)]).

title_line(_) -->
    % maybe it has no title?
    [].

item_line(Item, User, tree) -->
    html(
	div(class(item_line),
	    \spaced([
			   by_link(Item),
			   item_age_link(Item),
			   edit_link(Item, User),
			   delete_link(Item, User),
			   reply_link(Item)]))).

item_line(Item, User, _) -->
    html(
	div(class(item_line),
	    \spaced([
			   on_link(Item),
			   by_link(Item),
			   item_age_link(Item),
			   parent_link(Item),
			   comments_link(Item),
			   %      views_link(Item),
			   edit_link(Item, User),
			   delete_link(Item, User),
			   reply_link(Item)]))).

comments_link(Item) -->
    { http_link_to_id(item_handler, [id(Item)], Url),
      findall(Descendant,(ancestor_descendant(Item,Descendant),item_text(Descendant,_)),Descendants),
      length(Descendants,Number_of_descendants),
      ( Number_of_descendants > 0 ->
	plural(comment,Number_of_descendants,Comments_form),
	atomics_to_string([Number_of_descendants, " ", Comments_form], Comment_label);
	Comment_label = discuss) },
    html(['| ', a(href(Url),span(Comment_label))]).

views_link(Item) -->
    { findall(Item, user_seen(_,Item), Items),
      length(Items,Views) },
    html(['| ',Views,' views']).

edit_link(Item, User) -->
    { ground(User),
      user_can_edit_item(User, Item),
      http_link_to_id(edit_handler, [id(Item)], Url) },
    html(['| ',a(href(Url),span(edit))]).

edit_link(_, _) -->
    [].

delete_link(Item, User) -->
    { ground(User),
      user_can_delete_item(User, Item),
      http_link_to_id(delete_handler, [id(Item)], Url) },
    html(['| ',a(href(Url),span(delete))]).

delete_link(_, _) -->
    [].

by_link(Item) -->
    { item_author(Item, Author),
      http_link_to_id(news_handler, [by(Author)], Url) },
    html(a(href(Url), span(['by ', Author, ' ']))).

by_link(_) -->
    [].

item_age_link(Item) -->
    { text_age(Item, Age_),
      flatten([Age_, ' ago'], Age),
      http_link_to_id(item_handler, [id(Item)], Url) },
    html(a(href(Url), span(Age))).

parent_link(Item) -->
    { parent_child(Parent, Item),
      http_link_to_id(item_handler, [id(Parent)], Url) },
    html(['| ',a(href(Url), span(parent))]).

parent_link(_) -->
    [].

on_link(Item) -->
    { root_item(Root, Item),
      Root \= Item,
      item_title(Root, Title),
      http_link_to_id(item_handler, [id(Root)], Url) },
    html(a(href(Url), span([' on ', Title, ' ']))).

on_link(_) -->
    % if root is deleted / has no title
    [].

reply_link(Item) -->
    { http_link_to_id(reply_handler, [to(Item)], Url) },
    html(['| ',a(href(Url),span(reply))]).


% # VOTING
% ## Handling vote requests

vote_handler(post,Request):-
    % after voting, redirect back to the same page.
    % (if there's no referer for some reason, then just go to the front page.)
    ( member(referer(Referer), Request) -> Then=Referer; Then=root(.)),
    (get_user(User) ->
	 http_read_data(Request, Data, []),
	 Data = [id = Item_raw, how = How|_],
	 atom_number(Item_raw, Item),
	 process_vote(User, How, Item),
	 http_redirect(see_other, Then, Request);
     http_redirect(see_other, login, Request)).

process_vote(User, How, Item) :-
    % fails if item is by user
    not(item_author(Item, User)),
    % has user already voted for this item?
    %get_time(Time),
    (user_vote_item(User, How_then, Item) ->
	 retract_user_vote_item(User, _, Item),
	 % if the vote is a different kind, than the previous one, then still log it
	 % ( this code looks ugly, but makes the vote toggle work behave as expected )
	 ( How_then \= How -> assert_user_vote_item(User, How, Item); true);
     % TODO: should the IP of a vote also be stored?
     assert_user_vote_item(User, How, Item)).

% ## Displaying vote arrows
vote_arrows(Item) -->
    { item_score(Item, Score) },
    html(
	span(class(voting),[
		\vote_arrow(Item, up),
		span(class(score),Score),
		\vote_arrow(Item, down)])).

vote_arrow(Item, _) -->
    % no vote arrow for item author
    { get_user(User), item_author(Item, User),
      arrow(_, self, Arrow) },
    html(
	span(input([type(submit), class(arrow), value(Arrow), disabled]))).

vote_arrow(Item, How) -->
    % has the user already voted for this item?
    { (get_user(User), user_vote_item(User, How, Item) ->
	   State=voted;
       State=fresh),
      arrow(How, State, Arrow) },
    html(
	form([action(vote),method(post)],[
		 input([type(hidden),name(id),value(Item)]),
		 input([type(hidden),name(how),value(How)]),
		 input([type(submit),class(arrow), title(How), value(Arrow)])])).

% ## pagination

page_of_items(Items, Current_page, Per_page, Page_of_items, Total_pages) :-
    Start #= (Current_page - 1) * Per_page,
    End #= (Current_page) * Per_page,
    Position in Start..End,
    findall(
	Item,
	(label([Position]), nth1(Position, Items, Item)),
	Page_of_items),
    length(Items,Items_no),
    Total_pages #= (Items_no - 1) // Per_page + 1.

:- public pagination//3.
pagination(Current, Last, Params) -->
    { page_link_range(Current, Last, Range) },
    html(div(id(pagination),\page_links(Range, Current, Params))).

page_link_range(Current, Last, Range) :-
    Begin #>= 1,
    End #=< Last,
    Width #= 5,
    Begin #> Current - Width,
    End #< Current + Width,
    Page #>= Begin #\/ Page #= 1,
    Page #=< End #\/ Page #= Last,
    Page in 1..Last,
    findall(Page, label([Page]), Range).

page_links([], _, _) -->
    [].

page_links([Current|Pages], Current, Params) -->
    html(button(disabled,Current)),
    page_links(Pages, Current, Params).

page_links([Page|Pages], Current, Params) -->
    { merge_options([page(Page)],Params,New_params),
      http_link_to_id(news_handler, New_params, Url) },
    html(a(href(Url),button(Page))),
    page_links(Pages, Current, Params).
