:- module(about, []).
:- use_module(library(http/http_dispatch)).
:- use_module(markdown).

:- http_handler(root(about), about_handler, []).

about_handler(_Request):-
  read_file_to_codes("README.md",C,[]),
  atom_codes(A,C),
  markdown_html(A,About),
  reply_app_page(about,About).
