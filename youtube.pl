:- module(youtube, []).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_parameters)).

:- use_module(library(http/json_convert)).

:- use_module(library(http/html_head)).
:- use_module(library(http/html_write)).

:- http_handler(root(watch), watch_handler, [youtube//1]).


youtube(Yt_link) -->
    { process_create(
	  path('youtube-dl'),
%	  ['--prefer-free-formats','-e','-g',Yt_link],
%	  ['-fworst','--get-title','--get-url',Yt_link],
	  % read json!
	  ['-j',Yt_link],
	  [stdout(pipe(Ytdl_out_stream))]),
      read_string(Ytdl_out_stream, _, Ytdl_out_string),
      json_to_prolog(Ytdl_out_string, Ytdl_out)
%      split_string(Out_string, "\n", "", [Video_title,Video_src|_])
    },
    html([h1(foo_title),
	  p(Ytdl_out),
	  video([controls(controls),autoplay(autoplay)],
		[source(src(foo_link),'')])]).

watch_handler(Request):-
    http_parameters(Request,
		    [video(Yt_link,[])]),
    reply_html_page([],\youtube(Yt_link)).
