:- module(md, [md_dom/2, document//1]).
%:- set_prolog_flag(double_quotes, chars).
:- set_prolog_flag(double_quotes, string).

% ?- phrase(([alpha],list(Mid),[omega]), [alpha,beta,gamma,omega]).
%@ Mid = [beta, gamma] ;
%@ false.

% ?- phrase(([*],list(Text),[*],[foo]), [W]).
%@ false.
md_dom(Md0, Body) :-
  string_chars(Md0,Md1),
  phrase(document(Body),Md1).

document([]) --> [].

document([E|Es]) -->
    tag(E),
    document(Es).

% split_string(+String, +SepChars, +PadChars, -SubStrings)
tag(em(S)) -->
    { atomic_list_concat(['*',S,'*'],S1) },
    tag(S1).

tag(code(S)) -->
    ["`",S,"`"].

tag(S) -->
    [S].

% ?- md_dom(["*", foo, "*"], D).

% ?- md_dom(["* foo *"], D).
