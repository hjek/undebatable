:- module(upload, [
	      files_list//1,
	      upload_form//0,
	      upload_link//0
	  ]).

:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_header)).
:- use_module(library(http/http_client)).
:- use_module(library(http/http_server_files)).
:- use_module(library(http/html_write)).
:- use_module(library(option)).
:- use_module(library(persistency)).

:- http_handler(root(upload),
		upload(Method),
		[method(Method), method(get,post)]).

upload_link -->
    html(span(a(href(upload), upload))).

:- make_directory_path('www/upload').
http:location(upload, root(upload), []).
user:file_search_path(upload, 'www/upload').
:- http_handler(upload(.), serve_files_in_directory(upload), [prefix]).

%file_mime_type('foo.js', X).`

:- html_meta upload_form(?,?).
upload_form -->
    html(form([
		     method(post),
		     action(location_by_id(upload)),
		     enctype('multipart/form-data')],
	      [input([type(file), multiple, required, name(file)]),
	       input([type(submit), value(upload)])])).

files_list([]) -->
    [].

files_list([Filename|Filenames]) -->
    {re_match('^\\.',Filename)},
    files_list(Filenames).

files_list([Filename|Filenames]) -->
    {path_segments_atom(/(upload, Filename),Url)},
    html([
		li(a(href(Url),Filename)),
		\files_list(Filenames)]).

upload(get, _Request) :-
    directory_files('www/upload', Files),
    app:reply_app_page(
	upload,
	% TODO: why do I need to specify module here,
	% and not in other calls to reply_app_page elsewhere?
	% ... and only before [upload] is loaded manually ??
	[\(upload:upload_form), 
	hr(''),
	\(upload:files_list(Files))]).

upload(post, Request) :-
    http_read_data(Request, _Parts, [on_filename(save_file)]),
    http_redirect(see_other, root(upload), Request).

save_file(Stream, file(Filename, File), Options) :-
    option(filename(Filename), Options),
    path_segments_atom(/(/(www, upload), Filename),Target),
    setup_call_cleanup(
	open(Target, write, File,[encoding(octet)]),
	copy_stream_data(Stream, File),
	close(File)).

:- make_directory_path('www/upload').
:- db_attach('www/upload.pl', []).
