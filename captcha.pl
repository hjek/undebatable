:- module(captcha, [captcha/3]).

% requires ImageMagick
:- dynamic captcha/3.

% for item_title
%:- use_module(news).

word(Word) :-
  % pick a random word from a message
  findall(Text, news:item_title(_, Text), Texts),
  random_member(Random_text, Texts),
  split_string(Random_text, ' ', ' ,', Words),
  random_member(Word_s, Words),
  atom_string(Word, Word_s),
  % only allow latin letters in captcha
  re_match('^[a-zA-Z]+$', Word).

word(Word) :-
  % fallback words
  Words = [apple,banana,pear,broccoli,carrot,chili],
  random_member(Word,Words).

captcha(Id,Text,Image):-
  % if there's no text given, find a new word
  ( not(ground(Text)) ->
    word(Text)),
  % check if installed:
  Exe = path(convert),
  absolute_file_name(Exe,Exe_path), exists_file(Exe_path),
  random_between(-70,70,Swirl),
  random_between(30,100,Point),
  random_between(-30,30,Shear_x),
  random_between(-30,30,Shear_y),
  format(atom(Shear),'~wx~w',[Shear_x,Shear_y]),
  random_member(Font,['DejaVu-Sans-Bold','DejaVu-Serif']),
  uuid(Id),
  format(atom(Label),'label:~w',[Text]),
  process_create(Exe,[
    '-font',Font,
    '-pointsize',Point,
    Label,
    '-swirl',Swirl,
    '-shear',Shear,
    '-liquid-rescale','200x50!',
    '-threshold','50%',
    'inline:png:-'
  ],
  [stdout(pipe(Out))]),
  read_stream_to_codes(Out,Codes), atom_codes(Image,Codes),
  % no need to assert the b64 encoded image, right?
  % will just take up space
  assert(captcha(Id,Text,_)).
