:- module(app, [serve/1, reply_app_page/2]).

:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_header)).
:- use_module(library(http/http_client)).
:- use_module(library(http/html_head)).
:- use_module(library(http/html_write)).
:- use_module(library(http/http_server_files)).
:- use_module(library(http/http_wrapper)).

:- use_module(library(http/http_log)).

:- use_module(login).
:- use_module(about).

serve(Port) :-
  http_server(http_dispatch, [port(Port)]).

user:file_search_path(static, static).
:- http_handler(root(.), serve_files_in_directory(static), [prefix]).

csp(Content, Policy) -->
  html(meta([
    'http-equiv'('Content-Security-Policy'),
    content([Content, Policy])])).

reply_app_page(Title,Body):-
  reply_html_page(
    [title(Title),
     \html_requires(root('news.css')),
     % don't include resources from other sites
     \csp('default-src', "'self' data:;"),
     %\csp('default-src', "'none';"),
     \csp('style-src', "'self';"),
     % don't run unsafe scripts in messages, if they should ever sneak in there
     \csp('script-src', "'none';"),
     \csp('img-src', "'self' data:;")],
    Body).

