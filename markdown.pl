:- module(markdown, [markdown_html/2]).

% use external markdown
% TODO: write own markdown parser
% like https://gist.github.com/jbroadway/2836900

% workaround for weird bug on empty text
markdown_html('',[]).
markdown_html([],[]).

markdown_html(Text,Html) :-
  % check if installed:
  Exe = path(markdown),
  absolute_file_name(Exe,Path), exists_file(Path),
  % parse markdown
  process_create(Exe,['-s',Text,'-f','nopants,autolink,noimage,nohtml'],[stdout(pipe(Out))]),
  load_structure(stream(Out),Html,[dialect(html)]),!.

% fallback to plaintext if markdown processor ain't there
% (not dangerous because prolog escapes html tags)
markdown_html(Text,Text).

% markdown_html(Text,Html) :-
% % translate markdown to a term
%   process_create(path(markdown),['-t',Text],[stdout(pipe(Out))]),
%   read_stream_to_codes(Out,Codes),
%   atom_codes(Html,Codes).

%
